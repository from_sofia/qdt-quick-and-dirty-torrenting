#!/bin/bash

session=$RANDOM

# Create linode
linode-cli linodes create --type "g6-nanode-1" --region "eu-west" --image "linode/ubuntu21.04" --root_pass "YOUR_PASS_HERE" &> $PWD/.install_output-$session.txt

# Vars_01
output="$PWD/.install_output-$session.txt"
vps_ip=$(cat $output | grep linode | rev | awk '{print $2}' | rev)
vps_id=$(cat $output | grep linode | awk '{print $2}')
answer="NO"
check="RUNNING"
torrent_file=""
destination=""
dest_space=$(df $destination | tail -1 | awk '{print $4}')
file_size=""

# Check if VPS is online
while [ $answer="NO" ]
do
	if ping -c1 $vps_ip
	then
		answer="OK"
	else
		answer="NO"
	fi
done

# Expect script for SSH
cat >> /home/$USER/.exp_ssh_$session.sh << EOF
	#!/usr/bin/expect -f
	spawn ssh -D 17528 -l root $vps_ip
	expect "(yes/no/[fingerprint])? "
	send "yes"
	expect "*assword: "
	send "YOUR_PASS_HERE"
	expect "# "
	send "apt update ; apt install -y aria2"
EOF

# Open a detached screen session and execute the except script within it
chmod 0755 /home/$USER/.exp_ssh_$session.sh
screen -dmS mysess_$session
scrn_pid=$!
screen -S mysess_$session -p 0 -X stuff 'bash /home/$USER/.exp_ssh_$session.sh'

# While chromium is open keep tunnel open, then close it
firejail chromium --incognito --proxy-server="socks5://127.0.0.1:17528" &
chrm_pid=$!
while [ $check="RUNNING" ]
do
	if kill -0 $chrm_pid 
	then
		answer="RUNNING"
	else
		kill -9 $scrn_pid
		rm -f /home/$USER/.exp_ssh_$session.sh
		answer="NO"
	fi
done

# Vars_02
$torrent_file=$(ls -dtr /home/$USER/Downloads/* | tail -1)

# Move torrent to VPS and download it from there
cat >> /home/$USER/.exp_ssh_$session.sh << EOF
	#!/usr/bin/expect -f
	spawn scp $torrent_file root@$vps_ip:~/file.torrent
	expect "*assword: "
	send "YOUR_PASS_HERE"
	expect "*torrent*100%*"
	spawn ssh -l root $vps_ip
	expect "*assword: "
	send "YOUR_PASS_HERE"
	expect "# "
	send "mkdir -p /root/work/movie ; mv /root/file.torrent /root/work/"
	expect "# "
	send "aria2c /root/work/file.torrent --seed-time=0 --dir=/root/work/movie"
	expect "# "
	$file_size=$(send "du /root/work/movie | awk '{print $1}'")
	expect "# "
	send "exit"
EOF
bash /home/$USER/.exp_ssh_$session.sh

# Check if there is enough space on dest path
while $file_size -gt $dest_space
do
	read -p "There is not enough space on the specified destination, please ensure there are $file_size bytes or more free and try again. Has space been freed now? "
	echo 
	if [[ ! $REPLY =~ ^[Yy]$ ]]
	then
		continue
	fi
done

# Get media from VPS
cat >> /home/$USER/.exp_rsync_$session.sh << EOF
	#!/usr/bin/expect -f
	spawn rsync -avhe ssh root@$vps_ip:/root/work/movie/ $destination
	expect "*assword: "
	send "YOUR_PASS_HERE"
	expect ""
	send "exit 1"
EOF
bash /home/$USER/.exp_rsync_$session.sh

# Destroy VPS
read -p "Are you happy to destroy the linode now?" -n 1 -r
echo
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    exit 1
fi
linode-cli linodes delete $vps_id
rm -f $PWD/.install_output-$session.txt /home/$USER/.exp_*_$session.sh

# Bye greeting
echo -e "Operation has now been completed Pirate!\ndu -sh $destination/movie"
